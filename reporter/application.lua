-- settings
dofile('settings.lua')

-- board config
dofile('board.lua')

local debounceDelay = 50
local debounceAlarmId = 0
local lastInterrupt

local STATE_UNLOCKED = 1
local STATE_LOCKED = 2
local currentState
local managerConnected = false

local ws = websocket.createClient()


local function notifyLocked()
    print("locked")
    gpio.write(PIN_LED_GREEN, gpio.LOW)
    gpio.write(PIN_LED_RED, gpio.HIGH)
    if managerConnected then
        ws:send("busy")
    end
end

local function notifyUnlocked()
    print("unlocked")
    gpio.write(PIN_LED_RED, gpio.LOW)
    gpio.write(PIN_LED_GREEN, gpio.HIGH)
    if managerConnected then
        ws:send("free")
    end
end


local function getCurrentState()
    if gpio.read(PIN_SWITCH) == PIN_SWITCH_LOCKED then
        return STATE_LOCKED
    else
        return STATE_UNLOCKED
    end
end


local function notifyState()
    currentState = getCurrentState()
    if currentState == STATE_UNLOCKED then
        notifyUnlocked()
    else
        notifyLocked()
    end
end

local function interrupt(level, timestamp)
    gpio.trig(PIN_SWITCH)
    tmr.alarm(debounceAlarmId, debounceDelay, tmr.ALARM_SINGLE, function()
        gpio.trig(PIN_SWITCH, "both", interrupt)
        notifyState()
    end)
end

local function main()
    gpio.mode(PIN_LED_GREEN, gpio.OUTPUT)
    gpio.mode(PIN_LED_RED, gpio.OUTPUT)
    gpio.mode(PIN_SWITCH, gpio.INT)

    notifyState()
    gpio.trig(PIN_SWITCH, "both", interrupt)

    ws:on("connection", function(ws)
        print('got ws connection')
        managerConnected = true
        notifyState()
    end)

    ws:on("receive", function(_, msg, opcode)
        print('got message:', msg, opcode) -- opcode is 1 for text message, 2 for binary
    end)

    ws:on("close", function(_, status)
        print('connection closed', status)
        managerConnected = false
        ws = nil -- required to lua gc the websocket client
    end)

    ws:connect(MANAGER_URL)
end

main()
