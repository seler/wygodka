Installation
============

Get necessary tools
-------------------

    pip install nodemcu-uploader esptool

Flash nodemcu onto board
------------------------

.. code-block:: bash

    esptool --port /dev/ttyUSB0 write_flash -fm qio 0x0000 nodemcu-master-8-modules-2017-10-17-18-44-03-integer.bin

     
Configure
---------

.. code-block:: bash

    cp settings.lua{.example,}
    
and edit `settings.lua` to suit your needs.
It is especially crucial to set unique ID of the reporter.  

If you are using Witty Cloud dev module instead of custom board replace `board.lua` with `board.witty_cloud.lua` first.

Upload
------

Finally upload all files to nodemcu

.. code-block:: bash

    nodemcu-uploader upload *.lua
    
After reset wygodka reporter should automatically connect.
