

reporter
========

gówienko bazowane na układzie esp8266 z wgranym firmware nodemcu, który odpala lua. łączy się z wifi, następnie websocketem z managerem, raportuje stan łazienki w czasie rzeczywistym, tylko w momencie podłączenia się do websocketa lub zmiany stanu. diodami informuje:
* wolne - ciągłe zielone
* zajęte - ciągłe czerwone
* brak połączenia z wifi (także w trakcie łączenia?) - migające czerwone
* brak połączenia z mangerem (także w trakcie łączenia?) - migające zielone

wejściem do reportera jest krańcówka wciśnięta w futrynę drzwi, którą zamek po zamknięciu wciska.

manager
=======

serwer websocket, odbiera dane od reporterów, przesyła viewerom.

możliwe stany:
    offline
    wolne
    zajęte

viewer
======

przeglądarka statusów, pure js, skompilowany serwowany bezpośrednio z nginx.

dostaniemy rysunek, pionowy (na telefon), na kompie jakoś się to wyśrodkuje, będzie to schemat klatki schodowej z łazienkami, w jakiś sposób graficznie będzie oznaczona informacja o stanie łazienki.

łączy się z managerem po websockecie. po odpaleniu na telefonie dostajesz prompta zeby "zainstalować" skrót do apki na pulpicie. po kliknięciu na zajętą łazienkę dostajesz jednorazowo informację o jej zwolnieniu - jako notyfikację z html5. wykorzystac webworkery żeby działało to nawet po zamknięciu przeglądarki.

druga opcja to zamiast js zrobić w kotlin aplikację na androida.


co jest?
========

na razie mamy tylko proof of concept. 

na breadboardzie coś tam sklejone z przyciskiem, na nodemcu cos tam w lua, co łączy się z wifi i managerem, przesyła mu stan w czasie rzeczywistym.

manager to kilka linijek w pythonie z websockets.

viewer to kilka linijek w js, który łączy się z managerem i wyświetla wszystko co ten mu wyśle.


do zrobienia
============

* zlutować układ z nodemcu na płytce uniwersalnej lub drukowanej
* krańcówkę przylutować na kawałku płytki uniwersalnej, tak żeby dało się ją zamontować w futrynie i kabelkiem połączyć z ukłądem nodemcu
* napisać cały front w js, razem z powiadomieniami i działaniem w tle
* wdrożyć jakąś autentykację między reporterem a managerem (opcjonalnie?)

robota
======

proponuję podzielić się robotą w następujący sposób:
seler i jacek zajmą się raportowaniem stanu. w trakcie hackatonu obejmie to zamontowanie fizycznie układu w łazience oraz oprogramowanie w lua.

jagoda i łukasz zajmą się wyświetlaniem stanu, notyfikacjami i działaniem w tle.

u mnie w pokoju stoi raspberry ze stałym ip, na którym to wszystko postawimy. dam wam dostęp.

nazwa
=====

dobrze by było, żeby projekt się jakoś nazywał. moja propozycja to "wygódka", ale jestem otwarty na inne, przewijały się gdzieś nazwy szalet, wersal, maćku, łazienka jest zamknięta. tomek lewandowski da nam nazwę w domenie stxnext.local.
