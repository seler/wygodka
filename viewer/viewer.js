var window_dims = {
    't00':
        {
            'x': 0.695,
            'y': 0.85,
            'w': 0.143,
            'h': 0.033
        },
    't05':
        {
            'x': 0.694,
            'y': 0.692,
            'w': 0.145,
            'h': 0.1
        },
    't02':
        {
            'x': 0.183,
            'y': 0.692,
            'w': 0.145,
            'h': 0.1
        },
    't03':
        {
            'x': 0.695,
            'y': 0.49,
            'w': 0.145,
            'h': 0.1
        },
    't04':
        {
            'x': 0.185,
            'y': 0.49,
            'w': 0.145,
            'h': 0.1
        },
    't01':
        {
            'x': 0.695,
            'y': 0.333,
            'w': 0.145,
            'h': 0.1
        },
    't06':
        {
            'x': 0.183,
            'y': 0.333,
            'w': 0.145,
            'h': 0.1
        },
    't08':
        {
            'x': 0.695,
            'y': 0.205,
            'w': 0.145,
            'h': 0.045
        }
};

var State = {
    BUSY: 'busy',
    FREE: 'free'
}

var audio = new Audio('./audio/toilet.mp3');

var subscribedToilets = new Set();

function createCanvas(element) {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext("2d");
    var h = element.clientHeight,
        w = element.clientWidth;
    canvas.id = "canvas_id";
    canvas.setAttribute("class" ,"canvas");
    canvas.height = h;
    canvas.width = w;

    return canvas;
}

function notifyMe() {
  if (!("Notification" in window)) {
    console.log("This browser does not support system notifications");
  }

  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Notification enabled");
      }
    });
  }

}

function createNotification(body, title) {
    var options = {
        body: body,
        icon: './img/toilet.png',
    }
    var n = new Notification(title, options)
    setTimeout(n.close.bind(n), 5000);
}

function notificationEnabled() {
    return Notification.permission === 'granted';
}

function colorDatWindow(canvas, window_id, state) {
    var dat_window = window_dims[window_id];
    var color;
    if (state === "busy"){
        color = "#FF0000";
    }
    else if (state === "free") {
        color = "#00CC00";
    }

    drawReact(canvas, dat_window, color);
}

function drawReact(canvas, dat_window, color) {

    var ctx = canvas.getContext("2d");
    ctx.fillStyle = color;
    canvas_height = canvas.height;
    canvas_width = canvas.width;
    ctx.fillRect(
        dat_window['x']*canvas_width,
        dat_window['y']*canvas_height,
        dat_window['w']*canvas_width,
        dat_window['h']*canvas_height
    );

}

function notify(toiletId, state) {
    if (!subscribedToilets.has(toiletId)) {
        return;
    }
    if (state === State.BUSY) {
        createNotification('Niech żyje Król !', `Toaleta ${toiletId} jest zajeta`);
        subscribedToilets.delete(toiletId);
    } else if (state === State.FREE) {
        createNotification('Gra o Tron rozpoczęta !', `Toaleta ${toiletId} jest wolna`);
        audio.play();
    }
}

function setupWindow() {
    var element = document.getElementById('test');
    var canvas =  createCanvas(element);
    document.body.appendChild(canvas);
}

function setupResizeEvent() {
    window.addEventListener("resize", function() {
        var oldCanvas = document.getElementById('canvas_id');
        document.body.removeChild(oldCanvas);
        setupWindow()
    }, false);
}

function setupSockets() {
    var ws = new WebSocket("ws://10.93.1.197:5678/view");
    ws.onmessage = function (event) {
        var states = JSON.parse(event.data);
        for (toiletId in states){
            let state = states[toiletId];
            if (state !== "offline") {
                colorDatWindow(document.getElementById("canvas_id"), toiletId, state);
                if (notificationEnabled()) {
                    notify(toiletId, state);
                }
            }
        }
    };
}

function setupSubscriptions() {
    function handler(event){
        var canvas = document.getElementById("canvas_id");
        var x = event.pageX / canvas.width;
        var y = event.pageY / canvas.height;
        for (toiletId in window_dims) {
            var winDim = window_dims[toiletId];
            if (x > winDim.x && x < winDim.x + winDim.w) {
                if (y > winDim.y && y < winDim.y + winDim.h) {
                    subscribedToilets.add(toiletId);
                    drawReact(canvas, winDim, '#ffa500')
                    createNotification(`Toaleta ${toiletId} zostala zasubskrybowana`, 'Subskrypcja')
                }
            }
        }
    }
    window.addEventListener('click', handler, false);
    //window.addEventListener('touchstart', function(){handler(window.event)}, false);
    window.addEventListener('touchstart', handler, false);
}

window.onload = function() {
    setupSockets();
    setupWindow();
    notifyMe();
    setupResizeEvent();
    setupSubscriptions();
}
