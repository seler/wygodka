import './style.css';

import React from 'react';
import ReactDom from 'react-dom';

import {Provider} from 'react-redux'
import {store} from './store'
import Ljz from './ljz_container'

ReactDom.render(
    <Provider store={store}>
    <section>
        <h1>Łazienka jest zajęta</h1>
        <Ljz />
    </section>
    </Provider>
    ,
    document.getElementById('app-root')
)
