import React, { Component } from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {newMessage, subscribe } from './ljz_duck'
import {Ljz} from './ljz'

const mapStateToProps = (state, ownProps) => ({
    ...ownProps,
    state: state.ljz.state,
})

const mapDispatchToProps = (dispatch, ownProps) => bindActionCreators(
    {
        newMessage,
        subscribe,
    },
    dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Ljz)
