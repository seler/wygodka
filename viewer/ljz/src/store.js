import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import ljz from './ljz_duck'

const initialState = {}

const loggerMiddleware = store => next => action => {
    console.log('ACTION',action);
    var state = next(action)
    console.log('STATE',state);
    return state;
}

const rootReducer = combineReducers({
    ljz,
})

export const store = createStore(rootReducer,initialState,applyMiddleware(
    loggerMiddleware,
    thunkMiddleware,
))
