const NEW = 'LJZ/NEW'
const SUBSCRIBE = 'LJZ/SUB'

const initialState = {
    state: null
}

const socket = new WebSocket("ws://127.0.0.1:8000/ws");

export default (state = initialState, action) => {
    switch (action.type) {
        case NEW:
            return {state: action.payload}
        case SUBSCRIBE:
            return {state: 'SUBSCRIBED'}
        default:
            return state
    }
    return state
}

export const newMessage = data => ({
    type: NEW,
    payload: data.state
})


export const subscribe = () => (dispatch) => {
    console.log('subscribe');
    socket.onmessage = (event) => {
        const data = JSON.parse(event.data);
        console.log(data);
        if (data) {
            dispatch(newMessage(data));
        }
    };

    return {
        type: SUBSCRIBE,
        payload: {}
    }

}
