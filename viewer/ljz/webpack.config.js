var path = require('path')
var ExtractText = require('extract-text-webpack-plugin')
var HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: [
        './src/main.jsx'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.jsx', '.js']

    },
    devtool: 'sourcemap',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ExtractText.extract({
                    use: 'css-loader'
                })
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: {
                    loader: 'file-loader',
                    options:{
                        name: 'assets/[name].[hash].[ext]'
                    }
                }
            }
        ]
    },
    plugins: [
        new ExtractText({filename: 'bundle.css'}),
        new HTMLWebpackPlugin({template: './src/index.html'})
    ],
    devServer: {
        proxy: {
            //api 
        }
    }
}