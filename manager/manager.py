import asyncio
import datetime
import enum
import json
import os

import websockets
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import Column
from sqlalchemy.types import DateTime, Enum, Integer, String

path = os.path.dirname(os.path.abspath(__file__))
engine = create_engine('sqlite:///' + os.path.join(path, 'wygodka.db'))
Base = declarative_base()

Session = sessionmaker(bind=engine)


class Event(enum.Enum):
    lock = 1
    unlock = 2

    def log(self, data):
        data = json.dumps(data)
        event = Event_(type=self, timestamp=datetime.datetime.now(), data=data)
        session = Session()
        session.add(event)
        session.commit()


class Event_(Base):
    __tablename__ = 'event'

    id = Column(Integer, primary_key=True)
    type = Column(Enum(Event))
    timestamp = Column(DateTime)
    data = Column(String)

    def __repr__(self):
       return f"<Event(type='{self.type}', timestamp='{self.timestamp}'>"


Base.metadata.create_all(engine)


STATE_OFFLINE = 'offline'
STATE_ONLINE = 'online'
STATE_FREE = 'free'
STATE_BUSY = 'busy'
REPORTER_IDS = (
    't00',
    't01',
    't02',
    't03',
    't04',
    't05',
    't06',
    't08',
)
state = {id: STATE_OFFLINE for id in REPORTER_IDS}

viewers = set()


async def report_handler(websocket, path):
    if not path.startswith('/report'):
        return

    global reporters
    global state
    global viewers

    id = path.split('/')[2]
    state[id] = STATE_ONLINE
    try:
        while True:
            try:
                message = await asyncio.wait_for(websocket.recv(), timeout=20)
            except asyncio.TimeoutError:
                # No data in 20 seconds, check the connection.
                try:
                    await asyncio.wait_for(websocket.ping(), timeout=10)
                except asyncio.TimeoutError:
                    # No response to ping in 10 seconds, disconnect.
                    break
            else:
                print(id, message)

                if message == STATE_FREE:
                    Event.unlock.log({'id': id})
                elif message == STATE_BUSY:
                    Event.lock.log({'id': id})

                state[id] = message
                if viewers:
                    asyncio.ensure_future(asyncio.wait([ws.send(json.dumps(state)) for ws in viewers]))
    except websockets.exceptions.ConnectionClosed:
        pass
    finally:
        state[id] = STATE_OFFLINE
        if viewers:
            asyncio.ensure_future(asyncio.wait([ws.send(json.dumps(state)) for ws in viewers]))


async def view_handler(websocket, path):
    if path.startswith('/view'):
        global viewers
        # Register.
        viewers.add(websocket)
        try:
            await websocket.send(json.dumps(state))
            while True:
                await asyncio.sleep(10)
                try:
                    await asyncio.wait_for(websocket.ping(), timeout=10)
                except asyncio.TimeoutError:
                    # No response to ping in 10 seconds, disconnect.
                    break
        except websockets.exceptions.ConnectionClosed:
            pass
        finally:
            # Unregister.
            viewers.remove(websocket)


async def handler(websocket, path):
    report_task = asyncio.ensure_future(report_handler(websocket, path))
    view_task = asyncio.ensure_future(view_handler(websocket, path))
    done, pending = await asyncio.wait(
        [report_task, view_task],
        return_when=asyncio.ALL_COMPLETED,
    )

    for task in pending:
        task.cancel()


start_server = websockets.serve(handler, '0.0.0.0', 5678)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
